import fs from "fs";

import fetch from "node-fetch";

const generateRandomString = () => {
  return new Promise((resolve, reject) => {
    resolve((Math.random() + 1).toString(36).substring(7));
  });
};
const writeImagePromise = (imgbody, location) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(location + "robo.png", imgbody, (err) => {
      if (err) {
        console.log("not able to write inside the file");
      }
      resolve();
    });
  });
};
const DownloadRoboImage = async () => {
  try {
    const randomString = await generateRandomString();
    console.log(randomString);
    const resp = await fetch(`https://robohash.org/${randomString}`);
    //console.log(resp);
    const imgArrayBuffer = await resp.arrayBuffer();
    const imgData = Buffer.from(imgArrayBuffer);
    console.log(imgData);
    writeImagePromise(imgData, "./robotimg.txt");
    console.log("written successfully");
  } catch (err) {
    console.log(err);
  }
};
DownloadRoboImage();
