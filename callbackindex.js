import fs from "fs";
import superagent from "superagent";
const random = (Math.random() + 1).toString(36).substring(7);
superagent.get(`https://robohash.org/${random}`).then((res) => {
  console.log("robo image is ", res.request.url);
  fs.writeFile("./robotimg.txt", res.request.url, (err) => {
    if (err) {
      console.log("Not able to write to the file");
    }
    console.log("sucessfully written into the file");
  });
});
