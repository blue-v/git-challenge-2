import fs from "fs";
import superagent from "superagent";

const data = Math.random().toString(36).substring(2, 7);

function writeFilePromise(filelocation, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filelocation, data, (err) => {
      if (err) {
        reject("not able to write content in file");
      }
      resolve(data);
    });
  });
}

function RandomString(data) {
  console.log(`Random string is :${data}`);
}
superagent.get(`https://robohash.org/${data}`).end((err, res) => {
  if (err) {
    console.log("not able to retrive image", err);
    return;
  }
  const url = res.request.url.toString();
  console.log(url);
  return writeFilePromise("./RoboImage.txt", url);
});

RandomString(data);
