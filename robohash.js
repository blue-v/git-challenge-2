const generateRandomString = () => {
  return new Promise((resolve, reject) => {
    resolve((Math.random() + 1).toString(36).substring(7));
  });
};
generateRandomString();
